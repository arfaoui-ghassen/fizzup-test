<?php

namespace App\Http\Controllers;

use App\Http\Requests\AvisRequest;
use App\Models\Avis;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class FormController extends Controller
{
    public function store(AvisRequest $request)
    {
        try {
            $imageName = null;
            if ($request->hasFile('photo')) {
                $imageName = time() . '.' . $request->photo->extension();
                $request->photo->move(public_path('images'), $imageName);
            }

            $data = $request->all();
            $data['photo'] = public_path('images') . '/' . $imageName;

            $reviews = Avis::create($data);
            if ($reviews) {
                return response()->json([
                    'data' => $reviews,
                    'error' => false
                ]);
            } else {
                return response()->json([
                    'data' => [],
                    'error' => true
                ]);
            }

        } catch (\Exception $e) {
            return response()->json([
                'data' => [],
                'error' => true,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getReviews(Request $request)
    {
        if ($request->ajax()) {
            $data = Avis::get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('photo', function ($row) {
                    return $row->photo ? '<img src="' . $row->photo . '">' : null;
                })
                ->rawColumns(['photo'])
                ->make(true);
        }
    }

    public function index()
    {
        return view('reviews');
    }

    public function create()
    {
        return view('welcome');
    }
}
