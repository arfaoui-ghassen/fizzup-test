<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AvisRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'email' => 'required',
          'pseudo' => 'required',
          'note' => 'required|integer|between:1,10',
          'photo' => 'file|max:6144|mimes:jpg,bmp,png',
          'commentaire'=>"required "



        ];
    }
}
