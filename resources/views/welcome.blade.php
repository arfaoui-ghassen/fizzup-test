@extends('layout')
@section('content')

    <header class="ScriptHeader">
        <div class="rt-container">
            <div class="col-rt-12">
                <div class="rt-heading">
                    <h1>FizzUp Form </h1>
                </div>
            </div>
        </div>
    </header>

    <section>
        <div class="rt-container">
            <div class="col-rt-12">
                <div class="Scriptcontent">

                    <div class="feedback">
                        <div class="alert alert-danger" id="error_container" style="display:none">
                            <span id="error_message"></span>
                        </div>
                        <div id="alert"></div>
                        <p>Cher Client,<br><h4>Merci pour votre achat chez nous </h4>
                        </p>
                        Ajouter votre avis sur produit
                        <hr>

                        <form enctype="multipart/form-data" method="post" action="#" id="form_1">
                            <label>Email</label><br>
                            <input type="text" class="form-control" name="email"/>
                            <div class="clear"></div>
                            <hr class="survey-hr">

                            <label>Pseudo</label><br>
                            <input type="text" class="form-control" name="pseudo"/>


                            <div class="clear"></div>
                            <hr class="survey-hr">
                            <label>Note</label>

                            <input type="number" class="form-control" min="0" max="10" name="note"/>

                            <div class="clear"></div>
                            <hr class="survey-hr">
                            <label for="m_3189847521540640526commentText">Commentaire:</label><br/><br/>
                            <textarea id="myeditorinstance" class="form-control" cols="75" name="commentaire" rows="5">

                            </textarea>
                            <br>
                            <br>
                            <label>Attache Photo </label><br>
                            <input type="file" name="photo">

                            <hr class="survey-hr">

                            <div class="clear"></div>
                            <input style="background:#43a7d5;color:#fff;padding:12px;border:0" type="submit" id="submit"
                                   value="Envoyer">&nbsp;
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
