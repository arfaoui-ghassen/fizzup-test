@extends('layout')
@section('content')

    <header class="ScriptHeader">
        <div class="rt-container">
            <div class="col-rt-12">
                <div class="rt-heading">
                    <h1>FizzUp Form </h1>
                </div>
            </div>

        </div>
    </header>

    <section>

        <div class="rt-container">
            <div class="row">
                <div class="col-md-10 mb-2">
                    <div class="text-end">
                        <a href="{{route('reviews.create')}}" type="button" class="btn btn-lg btn-success">Ajouter Avis</a>
                    </div>
                </div>
            </div>
            <div class="col-rt-12">
                <div class="Scriptcontent">
                    <div class="feedback">
                        <div class="alert alert-danger" id="error_container" style="display:none">
                            <span id="error_message"></span>
                        </div>

                        <table class="table table-bordered yajra-datatable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Pseudo</th>
                                <th>Email</th>
                                <th>Note</th>
                                <th>Commentaire</th>
                                <th>Photo</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

@endsection
