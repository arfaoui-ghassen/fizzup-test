$(function () {
    const form = $('#form_1');
    const submitBtn = $('#submit');
    const errorContainer = $('#error_container');
    const errorMessage = $('#error_message');
    const alertBox = $('#alert');

    form.submit(function (e) {
        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        tinymce.triggerSave(); // update the original textarea with the current editor content

        const formData = new FormData(this);

        submitBtn.html('Please Wait...');
        submitBtn.attr('disabled', true);
        tinymce.activeEditor.setProgressState(true);
        $.ajax({
            url: '/api/avis',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (!response.error) {
                    document.getElementById('form_1').reset();
                    showSuccessAlert(alertBox);
                    setTimeout(function () {
                        window.location.href = '/';
                    }, 3000);
                }
            },
            error: function (error) {
                const err = error.responseJSON.errors;
                let errSpan = '';

                Object.keys(err).forEach(key => {
                    errSpan += `<span>${err[key][0]}</span><br>`;
                });

                errorContainer.show();
                errorMessage.html(errSpan);
            }
        }).always(function () {
            submitBtn.html('Submit');
            submitBtn.attr('disabled', false);
        });
    });

    const table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/reviews/list',
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'pseudo', name: 'pseudo'},
            {data: 'email', name: 'email'},
            {data: 'note', name: 'note'},
            {
                data: 'commentaire',
                name: 'commentaire',
                render: function (data, type, row, meta) {
                    return $('<div>').html(data).text();
                }
            },
            {data: 'photo', name: 'photo'},

        ]
    });

    function showSuccessAlert(alertBox) {
        alertBox.html('Merci pour votre Feedback :) ')
            .addClass('alert-in')
            .show();

        setTimeout(function () {
            alertBox.removeClass('alert-in').addClass('alert-out');

            setTimeout(function () {
                alertBox.hide().removeClass('alert-out');
            }, 500);
        }, 3000);
    }

    tinymce.init({
        selector: 'textarea#myeditorinstance',
        plugins: 'powerpaste advcode table lists checklist',
        toolbar: 'undo redo | blocks| bold italic | bullist numlist checklist | code | table'
    });
});
